README
======

Water level monitoring sensor node for `FloodMap.nz <https://floodmap.nz>`_.


Features
--------

* Works with `Kotahi.net <http://kotahi.net/>`_ LoRaWAN.
* Collects data from an ultrasonic distance sensor minutely, hourly, or daily.
* Goes to deep sleep mode when not collecting and transmitting data from the sensor.

-----

Hardware
--------

Sensor node can be build by putting together:

* `Adafruit Feather M0 Lora module <https://www.adafruit.com/products/3178>`_.
* `Ultrasonic module MC-SR04+ (3.3V/5V version) <https://www.aliexpress.com/item/Ultrasonic-Module-HC-SR04-Distance-Measuring-Transducer-Sensor-HC-SR04-perfect-upgrade-support-3-3V-work/32469473368.html>`_.
* 3.7V battery.

NOTE: **DO NOT** confuse `HC-SR04+` (3.3/5V) with the classic `HC-SR04` (5V only)! In other words, only `HC-SR04+` will work with the `Adafruit Feather` board.

-----

Software
--------

* Download and install `Arduino IDE <https://www.arduino.cc/en/Main/Software>`_.
* `Import the Adafruit boards <https://learn.adafruit.com/adafruit-feather-m0-radio-with-lora-radio-module/setup>`_.
* `Install <https://learn.adafruit.com/adafruit-all-about-arduino-libraries-install-use>`_ the `elapsedMillis <https://github.com/pfeerick/elapsedMillis>`_ library.
* Install `Mike Cochrane <http://mikenz.geek.nz/>`_'s fork of the `LoRa-LMIC-1.51 <https://github.com/mikenz/LoRa-LMIC-1.51>`_ library.
* Install patched version of `NewPing 1.8 <https://gitlab.com/markuz/NewPing-SAMD>`_ library with SAMD support.

-----

Configuration
-------------

Copy `configuration.h.example` to `configuration.h`.

Open `FloodMapNZ-SensorNode.ino` in the `Arduino IDE` which will also open `configuration.h` and `dtostrf.h`

All required configuration is in the `configuration.h`.

First, configure the `RTC` section. Uncomment one of the options to set period of data collection and trasmission. Eg. minutely::

    // {{{ RTC
    #define UPDATE_MINUTELY
    // #define UPDATE_HOURLY
    // #define UPDATE_DAILY
    // }}}


Then decide what features to use. Eg. keep the defaults in::

    // {{{ Features
    #define BATTERY_STATUS  // Battery voltage
    #define SENSOR_SR04PLUS // HC-SR04+
    // #define NODE_DEBUG      // Publish debug info via Serial
    // }}}


Then enter the `Device Address`, `Network Session Key` and `Application Session Key` (`ABP` details) for `FloodMap.nz` appliction that you received from `Kotahi.net <http://kotahi.net/connect/>`_.

For example, if you received the following `ABP` config::

    Device address: 01234567
    Network Session Key: 0123456789ABCDEF0123456789ABCDEF
    Application Session Key: 0123456789ABCDEF0123456789ABCDEF


You enter it to `configuration.h` as::

    // {{{ LoRaWAN
    // LoRaWAN Application identifier (AppEUI)
    static const u1_t AppEui[8] PROGMEM = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
    
    // Device Address
    devaddr_t DevAddr = 0x01234567;
    
    // Network Session Key
    unsigned char NwkSkey[16] = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF };
    
    // Application Session Key
    unsigned char AppSkey[16] = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF };
    // }}}


Lastly, you need to configure the sensor pins and max measurable distance in centimeters::

    // {{{ HC-SR04+
    #if defined(SENSOR_SR04PLUS)
    #define TRIGGER_PIN  12
    #define ECHO_PIN     11
    #define MAX_DISTANCE 400
    #endif
    // }}}


Once you have everything configured you can compile and upload the code via the `Arduino IDE` to the `Feather` board.

If you have configured the code correctly and you are in range of a `Kotahi.net` receiver then you will have sucessfully sent the data to `FloodMap.nz`.

-----

Wiring
------

.. image:: wiring/HC-SR04plus_bb.png
   :alt: FloodMap Sensor Node - Wiring Diagram

-----

3D prints
---------

* `MK1 - Top - Without Antenna - FloodMap.nz Box <https://tinkercad.com/things/aQKV0hN3IQS>`_ - default top with no hole for the antenna.
* `MK1 - Top - With Antenna - FloodMap.nz Box <https://tinkercad.com/things/a0evkVVzm1Y>`_ - top with a little hole for the antenna.
* `MK1 - Bottom - Base - FloodMap.nz Box <https://tinkercad.com/things/hQKN510brOI>`_ - default bottom with no attachment brackets.
* `MK1 - Bottom - ZipTie - FloodMap.nz Box <https://tinkercad.com/things/dRrrmhoZ9a7>`_ - bottom with brackets for zip ties.

-----

Photos
------

Few photos of `MK1` sensor nodes.

.. image:: photos/floodmapnz-sensor-nodes-mk1-1.jpg
   :alt: MK1 - without antenna

.. image:: photos/floodmapnz-sensor-nodes-mk1-2.jpg
   :alt: MK1 - inside

.. image:: photos/floodmapnz-sensor-nodes-mk1-3.jpg
   :alt: MK1 - with antenna
